FROM python:3
LABEL maintainer="Mikhail.K"
ENV ADMIN="MIXA"
# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1
#RUN mkdir django-react
WORKDIR /django_react
ADD ./project /
COPY ./requirements.txt .
CMD [ "bash", "./script.sh" ]
RUN pip install -r requirements.txt
EXPOSE 7000
EXPOSE 8000
#CMD [ "bash", "./script.sh" ]
