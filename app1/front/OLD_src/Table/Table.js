import React from 'react';

export default props => (
    <table className="table">
        <thead>
                <tr>
                  <th onClick={props.onSort.bind(null, 'id')}> 
                    ID {props.sortInField === 'id' ? <small>{props.sort}</small> : null} </th>
                  <th onClick={props.onSort.bind(null, 'firstName')}> 
                    First Name {props.sortInField === 'firstName' ? <small>{props.sort}</small> : null} </th>
                  <th onClick={props.onSort.bind(null, 'lastName')}> 
                    Last Name {props.sortInField === 'lastName' ? <small>{props.sort}</small> : null} </th>
                  <th onClick={props.onSort.bind(null, 'email')}> 
                    E-mail {props.sortInField === 'email' ? <small>{props.sort}</small> : null} </th>
                  <th onClick={props.onSort.bind(null, 'phone')}> 
                    Phone {props.sortInField === 'phone' ? <small>{props.sort}</small> : null} </th>
                </tr>

        </thead>
        <tbody>
            { props.data.map(item =>( <tr key={item.id + item.phone} onClick={props.selectRow.bind(null, item)}>
                    <td>{item.id}</td>
                    <td>{item.firstName}</td>
                    <td>{item.lastName}</td>
                    <td>{item.email}</td>
                    <td>{item.phone}</td>
                </tr>
            ))}
        </tbody>
    </table>
)
