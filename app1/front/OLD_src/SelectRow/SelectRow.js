import React from 'react';

export default ({item}) => (
   <div>
    <p>Выбран пользователь <b>{item.firstName + ' ' + item.lastName}</b></p>
    <p>
    Описание: <br />
    <textarea defaultValue={item.description} />
    </p>

    <p>Адрес проживания: <b>{item.address.streetAddress}</b></p>
    <p>Город: <b>{item.address.city}</b></p>
    <p>Провинция/штат: <b>{item.address.state}</b></p>
    <p>Индекс: <b>{item.address.zip}</b></p>
   </div>
)
