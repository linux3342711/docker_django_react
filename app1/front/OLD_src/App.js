import React, { Component } from 'react';
import Loader from './Loader/Loader.js';
import Table from './Table/Table.js';
import SelectRowView from './SelectRow/SelectRow.js';
import _ from 'lodash';


class Testo extends Component {
  state ={
    isLoading: true,
    data: [],
    sort: 'asc',
    sortInField: 'id',
    row: null,
  }

  async componentDidMount() {
    const otvet = await fetch(` http://www.filltext.com/?rows=32&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}`)
//    const otvet = await fetch(` http://192.168.217.129:7000/api/division`)
    const data = await otvet.json()
//    console.log(data)
    this.setState({
      isLoading: false,
      data: _.orderBy(data, this.state.sortInField, this.state.sort)
    })
  }

  onSort = sortInField => {
    const cloneData = this.state.data.concat();
    const sortMethod = this.state.sort === 'asc' ? 'desc' : 'asc';
    const orderedData = _.orderBy(cloneData, sortInField, sortMethod);

    this.setState({
      data: orderedData,
      sort: sortMethod,
      sortInField,
    })
  }

  selectRow = row => (
    //console.log(row)
    this.setState({row})
  )

  render() {
    return (
      <div className="container">
      {
        this.state.isLoading
        ? <Loader />
        : <Table
        data={this.state.data}
        onSort={this.onSort}
        sort={this.state.sort}
        sortInField={this.state.sortInField}
        selectRow={this.selectRow}
        />
      }


      {
        this.state.row ? <SelectRowView item={this.state.row} /> : null
      }
      </div>
    );
  }
}

export default Testo;
