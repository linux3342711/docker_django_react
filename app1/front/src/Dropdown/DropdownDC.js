import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

export default props => (
    <Dropdown    >
      <Dropdown.Toggle variant="primary" id="dropdown-basic" >
        ЦОД
      </Dropdown.Toggle>
        <Dropdown.Menu>
          {props.dataDC.map(item => ( <div key={item.id + item.region + item.datacenter} 
                                      onClick={props.getDC.bind(null, item.datacenter)}>

          <Dropdown.Item>{item.datacenter}</Dropdown.Item>
          </div>
          ))}
        </Dropdown.Menu>
    </Dropdown>


)
