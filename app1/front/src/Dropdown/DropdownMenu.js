import React from 'react';
//import Dropdown from 'react-bootstrap/Dropdown';
//import  './Table.css';

const DataCenter = ({ dataCenterOptions }) => {
  const mappedDropdownOptions = dataCenterOptions.map(item => {
    return (
      <div key={item.value} className="item">
        {item.value}
      </div>
    )
  })

  return (
    <div className="ui form">
      <div className="field">
        <label className="label">Select a Color</label>
        <div className="ui selection dropdown visible active">
          <i className="dropdown icon"></i>
          <div className="text">Select Color</div>
          <div className="menu visible transition">{mappedDropdownOptions}</div>
        </div>
      </div>
    </div>
  )
}

export default DataCenter
