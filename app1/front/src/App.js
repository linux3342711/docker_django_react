import React, { Component } from 'react';
import Loader from './Loader/Loader.js';
import TableA from './Table/TableA.js';
import TableC from './Table/TableC.js';
import TableD from './Table/TableD.js';
import SelectRowView from './SelectRow/SelectRow.js';
import DataCenter from './Dropdown/DropdownMenu.js';
import DC from './Dropdown/DropdownDC.js';
import WF from './WorkFlowNotes/WorkFlow.js';

import _ from 'lodash';
import ReactPaginate from 'react-paginate';
//import Combobox from "react-widgets/Combobox";
import Dropdown from 'react-bootstrap/Dropdown';


class Testo extends Component {
  state ={
    isLoading: true,
    isIncidents: true,
    data: [],
    dataC: [],
    dataD: [],
    dataDC: [],
    dataWF: [],
    dataBDC: [],
    sort: 'asc',
    sortInField: 'id',
    row: null,
    inc: null,
    param2: '',
    currentPage: 0,
// API
    api_ip: 'http://192.168.217.129:7000',
    url: '/api/division',
    urlC: '/api/incargs?ASSIGNMENT_GROUP=',
    urlBranch: '/api/divbr?branch=',
    urlDC: '/api/dc?branch=',
    urlWF: '/api/wf?ticket_id=',
    urlBranchDC: '/api/bdc?branch_dc=',
// data & vars
    direction: '',
    branch: '',
    dataCenter: [],
// current User
    c_user: 'Testov Testo Testovich',
  }

/*
  async componentDidMount() {
//    const otvet = await fetch(` http://www.filltext.com/?rows=32&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}`)
//    var url = "http://192.168.217.129:7000/api/division";
    const otvet = await fetch(this.state.api_ip + this.state.url,  )
    const data = await otvet.json()
    console.log(data)
    this.setState({
      isLoading: false,
      data: _.orderBy(data, this.state.sortInField, this.state.sort)
    })
  }
*/
  // тестовая функция по получению фильтра по Направлению РГ в первой таблице
  async getDataFromUrl(setUrl) {
    //this.state.url = setUrl

    const otvet = await fetch(setUrl  )
    const data = await otvet.json()
    this.setState({
      data: [],
      isLoading: false,
      data: _.orderBy(data, this.state.sortInField, this.state.sort),
    })
    console.log(data)
  }

  // тестовая функция по получению списка инцидентов по РГ
  async getDataFromUrlC(setUrl) {

    const otvetC = await fetch(setUrl  )
    const dataC = await otvetC.json()
    this.setState({
      dataC: [],
      isLoading: false,
      dataC: _.orderBy(dataC, this.state.sortInField, this.state.sort)
    })
  }

  // тестовая функция получения списка ЦОДов по выбранному филиалу
  async getDataFromUrlDC(setUrl) {

    const otvetDC = await fetch(setUrl  )
    const dataDC = await otvetDC.json()
    this.setState({
      dataDC: [],
      //isLoading: false,
      //console.log(dataDC),
      dataDC: _.orderBy(dataDC, this.state.sortInField, this.state.sort)
    })
    console.log(dataDC)
  }

  // тестовая функция получения списка РГ по выбранному филиалу и ЦОДу
  async getDataFromUrlBranchDC(setUrl) {

    const otvetBDC = await fetch(setUrl  )
    const dataBDC = await otvetBDC.json()
    this.setState({
      dataBDC: [],
      //isLoading: false,
      //console.log(dataDC),
      dataBDC: _.orderBy(dataBDC, this.state.sortInField, this.state.sort)
    })
    console.log(dataBDC)
  }


  // тестовая функция получения списка комментариев (заметок) по выбранному инциденту из WorkFlow
  async getDataFromUrlWF(setUrl) {

    const otvetWF = await fetch(setUrl  )
    const dataWF = await otvetWF.json()
    this.setState({
      dataWF: [],
      //isLoading: false,
      //console.log(dataDC),
      dataWF: _.orderBy(dataWF, this.state.sortInField, this.state.sort)
    })
    console.log(dataWF)
  }


  onSort = sortInField => {
    const cloneData = this.state.data.concat();
    const sortMethod = this.state.sort === 'asc' ? 'desc' : 'asc';
    const orderedData = _.orderBy(cloneData, sortInField, sortMethod);

    this.setState({
      data: orderedData,
      sort: sortMethod,
      sortInField,
    })
  }

  selectRow = row => (
    console.log(row),
    this.setState({row}),
    this.getDataFromUrlC(this.state.api_ip + this.state.urlC+row)
  )

  // выбор инцидента для вывода списка комментариев из WorkFlow
  selectInc = inc => (
    console.log(inc),
    this.setState({inc}),
    console.log(this.getDataFromUrlWF(this.state.api_ip + this.state.urlWF+inc))
  )

  // функция запроса по Направлению
  setDirection = url => (
    console.log(url),
    this.setState({url})
  )
  // pagination
  pageChangeHandler = ({selected}) => (
    this.setState({currentPage: selected})
  )

  // установка значения филиала, получение списка РГ и списка ЦОДов по филиалу
  setBranch = branch => (
  //console.log(branch),
  console.log(this.setState({branch})),
  this.getDataFromUrl(this.state.api_ip + this.state.urlBranch+branch),
  console.log(this.getDataFromUrlDC(this.state.api_ip+this.state.urlDC+branch))
  )

  //  получение списка РГ  по Филиалу и ЦОДу
  setDC = branch_dc => (
  //console.log(branch),
  //console.log(this.setState({branch})),
  console.log(this.getDataFromUrl(this.state.api_ip + this.state.urlBranchDC+this.state.branch+branch_dc))
  //console.log(this.getDataFromUrlDC(this.state.api_ip+this.state.urlDC+branch))
  )

  setDataCenter = dataCenter => (
    console.log(dataCenter),
//    console.log(this.getDataFromUrlBranchDC(this.state.api_ip + this.state.urlBranchDC+this.state.branch+dataCenter))
    console.log(this.getDataFromUrl(this.state.api_ip + this.state.urlBranchDC+this.state.branch+dataCenter))
  )

  // для внесения данных по Ходу Работ в Инциденте
  postWorkFlow = comment => (
  console.log(comment)
  )

  render() {
    // pagination
    const pageSize = 5;
    const filteredData = this.state.data;
    // debugger
    const pageCount = Math.ceil(filteredData.length / pageSize)
    const displayData = _.chunk(filteredData, pageSize)[this.state.currentPage]

    const dataCenterOptions = [{value: 'One'},{value: 'Two'},{value: 'Three'},{value: 'Four'},{value: 'Five'}];

    return (
      <main className="container-fluid">
        <h1 className="text-blue text-uppercase text-center my-1"> Проект ЕОУК {this.state.param2}</h1>
        <div className="row">
          <div className="col-lg-8 col-md-12 col-sm-10 mx-auto p-0">
            <div className="card p-3">
              <div className="mb-2 text-center">


                <div class="btn-group" role="group" aria-label="Basic">
                  <button type="button" class="btn btn-primary" 
                    onClick={() => console.log(this.getDataFromUrl('http://192.168.217.129:7000/api/divdir?direction=IT'))}>
                   
                   IT  
                  </button>
                  <button type="button" className="btn btn-disable" 
                  onClick={() => console.log(this.getDataFromUrlDC(this.state.api_ip+this.state.urlDC+this.state.branch))}>
                    CORE
                  </button>
                  <button type="button" class="btn btn-disable" onClick={() => console.log(this.state.isIncidents)}>TRM</button>
                  <button type="button" class="btn btn-disable" 
                    onClick={() => console.log(this.getDataFromUrl('http://192.168.217.129:7000/api/divdir?direction=IT'))}>RAN</button>
                  <button type="button" class="btn btn-disable">GOD</button>

                </div>
              </div>
        <div className="row">
          <div className="col-3">

    <Dropdown    >
      <Dropdown.Toggle variant="primary" id="dropdown-basic" >
        Филиал
      </Dropdown.Toggle>
        <Dropdown.Menu>
                <a class="dropdown-item" onClick={() => (this.setBranch('Столичный филиал')) } href="#">Столичный филиал</a>
                <a class="dropdown-item" onClick={() => (this.setBranch('Северо-Западный филиал')) } href="#">Северо-Западный филиал</a>
                <a class="dropdown-item" onClick={() => (this.setBranch('Дальневосточный филиал')) } href="#">Дальневосточный филиал</a>
                <a class="dropdown-item" onClick={() => (this.setBranch('Поволжский филиал')) } href="#">Поволжский филиал</a>
                <a class="dropdown-item" onClick={() => (this.setBranch('Кавказский филиал')) } href="#">Кавказский филиал</a>
                <a class="dropdown-item" onClick={() => (this.setBranch('Сибирский филиал')) } href="#">Сибирский филиал</a>
                <a class="dropdown-item" onClick={() => (this.setBranch('Уральский филиал')) } href="#">Уральский филиал</a>
        </Dropdown.Menu>
    </Dropdown>
          </div>


      <div className="col-3">
        {
         <DC
           dataDC={this.state.dataDC}
           getDC={this.setDataCenter}
           //setDC={this.setDC}
          />
        }
      </div>

          <div className="col-3">
            <div className="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton3" data-bs-toggle="dropdown" aria-expanded="false">
                Автозал
              </button>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Действие</a></li>
                <li><a class="dropdown-item" href="#">Другое действие</a></li>
                <li><a class="dropdown-item" href="#">Что-то еще здесь</a></li>
                <li><hr class="dropdown-divider" /></li>
                <li><a class="dropdown-item" href="#">Отделенная ссылка</a></li>
              </ul>
            </div>
          </div>

<div className="col-2">
            <div className="dropdown">
              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton4" data-bs-toggle="dropdown" aria-expanded="false">
                Стойка
              </button>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Действие</a></li>
                <li><a class="dropdown-item" href="#">Другое действие</a></li>
                <li><a class="dropdown-item" href="#">Что-то еще здесь</a></li>
                <li><hr class="dropdown-divider" /></li>
                <li><a class="dropdown-item" href="#">Отделенная ссылка</a></li>
              </ul>
            </div>
          </div>

            </div>
          </div>
        </div>
</div>

      <h2 className="text-blue text-center " style={{fontSize:19}}> Контейнер с сеткой </h2>
      
      <div className="col-md-12 " style={{fontSize:11}}>
      <div className="row">
      <div className="col-4">
      <h3 className="text-blue text-center"  style={{fontSize:17}}> Список сервисов </h3>

      {
        this.state.isLoading
        ? <Loader />
        : <TableA
          data={displayData}
          onSort={this.onSort}
          sort={this.state.sort}
          sortInField={this.state.sortInField}
          selectRow={this.selectRow}
          setDirection={this.setDirection}
          getDataFromUrlC={this.getDataFromUrlC}
        />
      }
      {
        this.state.data.length > pageSize
        ? <ReactPaginate
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.pageChangeHandler}
          containerClassName={'pagination'}
          activeClassName={'active'}
          pageClassName="page-item"
          pageLinkClassName="page-link"
          previousClassName="page-item"
          nextClassName="page-item"
          previousLinkClassName="page-link"
          nextLinkClassName="page-link"
          forcePage={this.state.currentPage}
        /> : null
      }


      {
        this.state.row ? <SelectRowView item={this.state.row} /> : null
      }
      </div>

      <div className="col-4 ">
      <button type="button" class="btn btn-primary" onClick={() => console.log(this.setState({isIncidents: false}))}>iFrame Jazz</button>
      <button type="button" class="btn btn-primary" onClick={() => console.log(this.setState({isIncidents: true}))}>ИНЦИДЕНТЫ</button>
      <div className="table-responsive-sm">
      {
        this.state.isIncidents
        ? <TableC
//        <TableC 
          dataC={this.state.dataC}
//        onSort={this.onSort}
//        sort={this.state.sort}
//        sortInField={this.state.sortInField}
          selectInc={this.selectInc}
        />
        : <iframe src="http://192.168.217.129:7000" width={500} height={1000} sandbox='allow-scripts allow-modal'></iframe>
      }
      </div>
      </div>
      <div className="col-1">
        <h3 className="text-blue text-center" style={{fontSize:17}}> Ручные заметки</h3>
        {
          <TableD
            dataWF={this.state.dataWF}
            //selectInc={this.selectInc}
          />
        }
        {
          this.state.inc 
          ? <WF 
            item={this.state.inc} 
            c_user={this.state.c_user}
            comment={this.postWorkFlow}
          /> : null
        }

        <div>
        </div>
      </div>

      <div className="col-4">
        <h3 className="text-blue text-center" style={{fontSize:17}}> # Зона "Г" </h3>

      </div>

    </div>
    </div>
    </main>
    );
  }
}

export default Testo;
