import React from 'react';

export default props => (
    <table className="table table-striped table-responsive-sm table-hover table-condensed table-sm ">
        <thead>
                <tr>
                  <th >
                    Время  </th>
                  <th > 
                    Содержание  </th>
                  <th > 
                    Автор  </th>
                  <th > 
                    Инцидент  </th>
                </tr>

        </thead>
        <tbody>
            { props.dataWF.map(item =>( 
               <tr key={item.work_begin_time + item.ticket_id} >
                    <td>{item.work_begin_time}</td>
                    <td>{item.comment_state}</td>
                    <td>{item.comment_author}</td>
                    <td>{item.ticket_id}</td>
                </tr>
            ))}
        </tbody>
    </table>
)
