import React from 'react';
import  './Table.css';

export default props => (
    <table className="table table-success table-hover table-striped size-sm ">
        <thead>
                <tr>
                  <th onClick={props.onSort.bind(null, 'is_importance')}>
                    ФКС {props.sortInField === 'is_importance' ? <small>{props.sort}</small> : null} </th>
                  <th onClick={props.onSort.bind(null, 'is_division')}> 
                    Отв РГ {props.sortInField === 'is_division' ? <small>{props.sort}</small> : null} </th>
                  <th onClick={props.onSort.bind(null, 'service_name')}> 
                    Сервис {props.sortInField === 'service_name' ? <small>{props.sort}</small> : null} </th>
                  <th onClick={props.onSort.bind(null, 'is_owner')}> 
                    Владелец ИС {props.sortInField === 'is_owner' ? <small>{props.sort}</small> : null} </th>
                </tr>

        </thead>
        <tbody >
            { props.data.map(item =>( <tr key={item.id + item.is_division + item.branch} onClick={props.selectRow.bind(null, item.is_division)}>
                    <td>{item.is_importance}</td>
                    <td>{item.is_division}</td>
                    <td>{item.service_name}</td>
                    <td>{item.is_owner}</td>
                </tr>
            ))}
        </tbody>
    </table>
)
