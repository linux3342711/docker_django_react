import os
import cx_Oracle as ora

ora_s = os.getenv("ORCL_HOST")
ora_u = os.getenv("ORCL_USER")
ora_p = os.getenv("ORCL_PASS")
ora_port = os.getenv("ORCL_PORT")
ora_service = os.getenv("ORCL_DB")

#print(ora_s, ora_u, ora_p, ora_port, ora_service)

print (f'''{os.getenv("ORCL_HOST")}:{os.getenv("ORCL_PORT")}/{os.getenv("ORCL_DB")}''')

def get_query():
    sql = "SELECT * FROM PERSONS"
#    print(ora_s, ora_u, ora_p, ora_port, ora_service)
#    print(f"{ora_s}:{ora_port}/{ora_service}")
    try:
        with ora.connect(user=ora_u, password=ora_p, dsn=f"{ora_s}:{ora_port}/{ora_service}", encoding="UTF-8") as connection:
            c = connection.cursor()
            for (id, f_name, l_name) in c.execute(sql):
                print(f_name + " " +  l_name)
    except Exception as e:
        print ("АЩИПКА: " + str(e))

get_query()
