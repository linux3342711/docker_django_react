from django.apps import AppConfig


class EoukConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'eouk'
