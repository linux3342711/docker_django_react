# Generated by Django 4.1.7 on 2023-04-14 06:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eouk', '0006_alter_branch_region_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='SmAdmin',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('NUMBER_IM', models.CharField(default='Any', max_length=24, verbose_name='Number IM')),
                ('BRIEFDESCRIPTION', models.CharField(default='Any', max_length=256, verbose_name='Brief Description')),
                ('CMP_STATUS', models.CharField(default='Any', max_length=256, verbose_name='CMP Status')),
                ('ASSIGNMENT_GROUP', models.CharField(default='Any', max_length=256, verbose_name='Assignment Group')),
                ('AFFECTED_ITEM', models.CharField(default='Any', max_length=256, verbose_name='Affected Item')),
            ],
            options={
                'verbose_name': 'SM_Admin',
                'verbose_name_plural': 'SM_Admins',
                'db_table': 't_smadmin_probsummarym1',
            },
        ),
    ]
