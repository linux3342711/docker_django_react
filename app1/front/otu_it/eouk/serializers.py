from rest_framework import serializers
from eouk.models import (Division, SmAdmin, Branch, Person, Position, SmAdminOther, 
                         SmAdminOtherDB, CacheVeiwFsmRep, PersonsOtherDB, Workflow, DivisionBranchDC, )

# сериализатор для рабочих групп
class DivisionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Division
        fields = ('id',
                  'is_division',
                  'service_name',
                  'direction',
                  'is_owner',
                  'is_importance',
                  'branch')
        #fields = '__all__'
        #exclude = ['branch']

# сериализатор оп сервису РГ
class DivisionServiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Division
        fields = ('service_name', )

    def get_service_name(self):
        return self.service_name

# сериализатор по филиалу РГ
class DivisionBranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Division
        fields = ('branch', )

    def get_branch(self):
        return self.branch

# сериализатор по филиалу для получения списка ЦОДов
class DatacenterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branch
        fields = '__all__'

    def get_branch(self):
        return self.datacenter

# сериализатор по Филиалу и ЦОДу для получения списка РГ
class DivisionBranchDCSerializer(serializers.ModelSerializer):

    class Meta:
        model = DivisionBranchDC
        fields = '__all__'

    def get_branch(self):
        return self.branch_dc

# сериализатор по инцидентам
class SmAdminSerializer(serializers.ModelSerializer):

    class Meta:
        model = SmAdmin
        fields = '__all__'

# сериализатор по инцидентам FROM OTHER schema
class SmAdminOtherSchemaSerializer(serializers.ModelSerializer):

    class Meta:
        model = SmAdminOther
        fields = '__all__'

# сериализатор по инцидентам FROM OTHER Database
class SmAdminOtherDBSerializer(serializers.ModelSerializer):

    class Meta:
        model = SmAdminOtherDB
        fields = '__all__'

# сериализатор по пользователям FROM local Oracle Database
class PersonsOtherDBSerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonsOtherDB
        fields = '__all__'


# test
class IncidentSerializer(serializers.ModelSerializer):
    division = serializers.CharField(source='ASSIGNMENT_GROUP')

    class Meta:
        model = SmAdmin
        fields = '__all__'

# Test persons on positions
class PositionSerializer(serializers.ModelSerializer):
    fio_name = serializers.CharField(source='fio.first_name', )

    class Meta:
        model = Position
#        fields = '__all__'
        fields = ('pos_name', 'division_name', 'fio_name', )


### FROM CACHE DB
class CacheVeiwFsmRepSerializer(serializers.ModelSerializer):

    class Meta:
        model = CacheVeiwFsmRep
        fields = '__all__'

class WorkflowSerializer(serializers.ModelSerializer):
    workflow = serializers.CharField(source='ticket_id')

    class Meta:
        model = Workflow
        fields = '__all__'
