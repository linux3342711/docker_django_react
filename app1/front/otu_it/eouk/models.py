from django.db import models
from datetime import datetime

# Create your models here.

class Division(models.Model):
    is_division = models.CharField(db_column='is_division', default='Any', max_length=512, null=False,  verbose_name='IS Division')
    service_name = models.CharField(db_column='service_name', default='Any', max_length=512,  verbose_name='Service Name')
    direction =  models.CharField(db_column='direction', default='Any', max_length=128,  verbose_name='Service')
    is_owner = models.CharField(db_column='is_owner', default='Any', max_length=512, verbose_name='IS Owner')
    is_importance = models.IntegerField(default=-1, null=True, )
    branch = models.CharField(db_column='branch', default='Any', max_length=256, verbose_name='Branch')

    class Meta:
        db_table = 't_dm_sm_divisions'
        verbose_name = ('Division')
        verbose_name_plural = ('Divisions')

    def __str__(self):
        return '%s, %s' % (self.is_division, self.branch )

class Branch(models.Model):
    branch = models.CharField(db_column='BRANCH', default='Any', max_length=256, null=False,  verbose_name='Branch')
    region = models.CharField(db_column='REGION', default='Any', max_length=256, null=False, verbose_name='Region')
    address =  models.CharField(db_column='ADDRESS', default='Any', max_length=2048,  verbose_name='Address')
    datacenter = models.CharField(db_column='DATACENTER', default='Any', max_length=2048, verbose_name='Datacenter')
    room = models.CharField(db_column='ROOM', default='Any', max_length=2048, null=True,verbose_name='Room')
    rack = models.CharField(db_column = 'RACK', default='Any', max_length=2048, null=True,  verbose_name='Rack')
    datacenterschemalink =  models.CharField(db_column='DATACENTERSCHEMALINK',null=True, default='Any', max_length=2048,  verbose_name='Datacenterschemalink')
    roomschemalink = models.CharField(db_column='ROOMSCHEMALINK', default='Any',null=True, max_length=2048, verbose_name='Roomschemalink')
    drpschemalink = models.CharField(db_column='DRPSCHEMALINK', default='Any', null=True, max_length=2048, verbose_name='drpschemalink')
    site_id = models.IntegerField(db_column='SITE_ID', default=-1, null=True, )
    region_id = models.IntegerField(db_column='REGION_ID', default=-1, null=True, )
    room_id = models.IntegerField(db_column='ROOM_ID', default=-1, null=True, )
    rack_id = models.IntegerField(db_column='RACK_ID', default=-1, null=True, )

    class Meta:
        db_table = 't_dm_nri_branch'
        verbose_name = ('Branch')
        verbose_name_plural = ('Branches')

    def __str__(self):
        return '%s' % self.branch


class SmAdmin(models.Model):
    NUMBER_IM = models.CharField( default='Any', max_length=24, null=False,  verbose_name='Number IM')
    BRIEFDESCRIPTION = models.CharField( default='Any', max_length=2048,  verbose_name='Brief Description')
    CMP_STATUS =  models.CharField( default='Any', max_length=256,  verbose_name='CMP Status')
    ASSIGNMENT_GROUP = models.CharField( default='Any', max_length=512, verbose_name='Assignment Group')
    AFFECTED_ITEM = models.CharField(default='Any', max_length=512, verbose_name='Affected Item')

    class Meta:
        db_table = 't_smadmin_probsummarym1'
        verbose_name = ('SM_Admin')
        verbose_name_plural = ('SM_Admins')

    def __str__(self):
        return '%s, %s' % (self.NUMBER_IM, self.ASSIGNMENT_GROUP)

# TEST
class Person(models.Model):
    first_name = models.CharField( default='Any', max_length=24, null=False,  verbose_name='Имя')
    second_name = models.CharField( default='Any', max_length=24, null=False,  verbose_name='Фамилия')

    class Meta:
        db_table = 't_persons'
        verbose_name = ('Person')
        verbose_name_plural = ('Persons')

    def __str__(self):
        return '%s, %s' % (self.first_name, self.second_name)

# TEST
class Position(models.Model):
    pos_name = models.CharField( default='Any', max_length=24, null=False,  verbose_name='Должность')
    division_name = models.CharField( default='Any', max_length=24, null=False,  verbose_name='Рабочая группа')
    fio = models.ForeignKey( Person, on_delete=models.CASCADE,  verbose_name='Имя сотрудника')

    class Meta:
        db_table = 't_positions'
        verbose_name = ('Position')
        verbose_name_plural = ('Positions')

    def __str__(self):
        return '%s, %s, %s' % (self.pos_name, self.division_name, self.fio)

class Workflow(models.Model):
    work_begin_time = models.DateTimeField(auto_now=True)
    comment_state = models.CharField( default='Any', max_length=4096, null=False,  verbose_name='Комментарии')
    comment_author = models.CharField( default='Any', max_length=256, null=False,  verbose_name='Автор комментария')
    ticket_id = models.CharField( default='Any', max_length=24, null=False,  verbose_name='Номер инцидента')

    class Meta:
        db_table = 't_dm_workflow'
        verbose_name = ('Workflow')
        verbose_name_plural = ('Workflows')

    def __str__(self):
        return '%s, %s, %s, %s' % (self.work_begin_time, self.comment_state, self.comment_author, self.ticket_id, )

class NriFmIt(models.Model):
    CI = models.CharField( default='Any', max_length=64, null=False,  verbose_name='КЕ')
    CI_ID = models.BigIntegerField( default=-1,   verbose_name='КЕ ID')
    divice_name = models.CharField( default='Any', max_length=512,  verbose_name='Название устройства')
    logical_status = models.CharField( default='Any', max_length=48, null=False,  verbose_name='Статус')
    is_division = models.CharField(db_column='is_division', default='Any', max_length=512, null=False,  verbose_name='IS Division')

    class Meta:
        db_table = 't_nri_fm_it'
        verbose_name = ('NRI_FM_IT')
        verbose_name_plural = ('NRI_FM_IT')

    def __str__(self):
        return '%s, %s, %s, %s, %s' % (self.CI, self.CI_ID, self.divice_name, self.logical_status, self.is_division )

## PROD Test local
# not managed view to select IS_DIVISION by BRANCH and DATACENTER
class DivisionBranchDC(models.Model):
    is_importance = models.CharField( )
    is_division =  models.CharField( )
    service_name = models.CharField( )
    is_owner = models.CharField( )
    branch_dc = models.CharField( )
#    DATACENTER = models.CharField( )

    class Meta:
        managed = False # create from a view of other Database
        db_table = '"public"."v_division_b_dc"'

#non managed table from other schema in current Database
class SmAdminOther(models.Model):
    number_im = models.CharField( )
#    BRIEFDESCRIPTION = models.CharField( default='Any', max_length=2048,  verbose_name='Brief Description')
    cmp_status =  models.CharField( )
    assignment_group = models.CharField( )
#    AFFECTED_ITEM = models.CharField(default='Any', max_length=512, verbose_name='Affected Item')

    class Meta:
        managed = False # create from a view of other Database
        db_table = '"test_schema"."t_smadmin_probsummarym1"'
        #verbose_name = ('SM_Admin')
        #verbose_name_plural = ('SM_Admins')

    #def __str__(self):
        #return '%s, %s' % (self.NUMBER_IM, self.ASSIGNMENT_GROUP)

### TEST not managed DB
#non managed table from other  Database
class SmAdminOtherDB(models.Model):
    number_im = models.CharField( )
    cmp_status =  models.CharField( )
    assignment_group = models.CharField( )

    class Meta:
        managed = False # create from a view of other Database
        db_table = '"public"."t_smadmin_probsummarym1"'

# LOCAL Oracle
class PersonsOtherDB(models.Model):
    FIRST_NAME = models.CharField( )
    SECOND_NAME =  models.CharField( )

    class Meta:
        managed = False # create from a view of other Database
        db_table = '"REMOTEUSER"."PERSONS"'

### NOT Managed Tables from Other DB
## FROM CACHE 
# incidents
#class SmAdminFsmRep(models.Model):
class CacheVeiwFsmRep(models.Model):
    NUMBER = models.CharField( )
    BRIEF_DESCRIPTION = models.CharField( )
    CMP_STATUS =  models.CharField( )
    ASSIGNMENT_GROUP = models.CharField( )
    AFFECTED_ITEM = models.CharField()
    OPEN_TIME = models.DateTimeField()

    class Meta:
        managed = False # create from a view of other Database
        db_table = '"CACHE"."V_T_R_PROBSUMMARYM1"'




