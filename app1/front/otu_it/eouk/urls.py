from django.urls import re_path, path
from eouk import views

urlpatterns = [
#    re_path(r'^api/division$', views.division_list),
    ### SIMPLE
    path('api/division/', views.DivisionListCreate.as_view() ),
    path('api/smadmin', views.MultiApiView.as_view()),
    path('api/incident', views.IncidentApiView.as_view()),
    ### CUSTOM
    ## кастомные АПИ для получения данных с параметрами
    path('api/incargs', views.IncidentArgsApiView.as_view()),
    path('api/divdir', views.DivisionByDirectionArgsApiView.as_view()),
    path('api/divbr', views.DivisionByBranchArgsApiView.as_view()),
    path('api/dc', views.DatacenterSerializerApiView.as_view()),
#    path('api/wf', views.WorkFlowArgsApiView.as_view()),
    path('api/wf', views.WorkFlowArgsApiView),
    path('api/bdc', views.DivisionBranchDCSerializerApiView.as_view()),
    ## TEST
    path('api/position', views.PositionApiView.as_view()),
    path('api/posargs', views.PositionArgsApiView.as_view()),
    path('api/otherDB', views.OtherDBIncidentApiView.as_view()),
    path('api/otherSchema', views.OtherSchemaIncidentApiView.as_view()),
    path('api/orclPersons', views.PersonsOtherDBApiView.as_view()),
    ### PROD Megafon
    ## FROM CACHE
    path('api/cacheInc', views.CacheVeiwFsmRepApiView.as_view()),
#    url(r'^api/tutorials/(?P<pk>[0-9]+)$', views.tutorial_detail),
#    url(r'^api/tutorials/published$', views.tutorial_list_published)
]
