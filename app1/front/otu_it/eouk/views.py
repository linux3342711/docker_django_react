from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status

from eouk.models import (Division, SmAdmin, Branch, Position, Person, SmAdminOther, SmAdminOtherDB, 
                         CacheVeiwFsmRep, PersonsOtherDB, Workflow, DivisionBranchDC, )
from eouk.serializers import (DivisionSerializer, DivisionServiceSerializer, SmAdminSerializer, 
                              IncidentSerializer, PositionSerializer, DivisionBranchSerializer, 
                              SmAdminOtherDBSerializer, SmAdminOtherSchemaSerializer, CacheVeiwFsmRepSerializer, 
                              PersonsOtherDBSerializer, DatacenterSerializer, WorkflowSerializer, 
                              DivisionBranchDCSerializer, )
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.db.models import Q # для сложного запроса типа ИЛИ

#
'''
@api_view(['GET'])
def division_list(request):
    divisions = Division.objects.filter(published=True)

    if request.method == 'GET':
        division_serializer = DivisionSerializer(divisions, many=True)
        return JsonResponse(division_serializer.data, safe=False)
'''

#
from rest_framework import generics
# весь список РГ
class DivisionListCreate(generics.ListCreateAPIView):
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer

# весь список по Инцидентам
class IncidentApiView(generics.ListCreateAPIView):
    queryset = SmAdmin.objects.all()
    serializer_class = IncidentSerializer

## TEST
# весь список по Инцидентам FROM OTHER schema
class OtherSchemaIncidentApiView(generics.ListCreateAPIView):
    queryset = SmAdminOther.objects.using('default')
    serializer_class = SmAdminOtherSchemaSerializer

## TEST
# весь список по Инцидентам FROM OTHER Database
class OtherDBIncidentApiView(generics.ListCreateAPIView):
    queryset = SmAdminOtherDB.objects.using('test_db')
    serializer_class = SmAdminOtherDBSerializer

## TEST
# весь список пользователей FROM OTHER Database - Local Oracle
class PersonsOtherDBApiView(generics.ListCreateAPIView):
    queryset = PersonsOtherDB.objects.using('local_orcl')
    serializer_class = PersonsOtherDBSerializer


## PROD Megafon
### FROM CACHE incidents
class CacheVeiwFsmRepApiView(generics.ListCreateAPIView):
    queryset = CacheVeiwFsmRep.objects.using('cache')
    serializer_class = CacheVeiwFsmRepSerializer

    def get_queryset(self, *args):
        query = self.request.GET.get('ASSIGNMENT_GROUP', *args)
        return super().get_queryset().filter(ASSIGNMENT_GROUP=query)

#
from drf_multiple_model.views import ObjectMultipleModelAPIView

class MultiApiView(ObjectMultipleModelAPIView):
    querylist = [
        {'queryset': SmAdmin.objects.filter(ASSIGNMENT_GROUP='ITi-Operating-Infra'), 'serializer_class': SmAdminSerializer},
#        {'queryset': SmAdmin.objects.filter(ASSIGNMENT_GROUP__in=select), 'serializer_class': SmAdminSerializer},
    ]

# API по должности FK к ФИО
class PositionApiView(generics.ListCreateAPIView):
    queryset = Position.objects.filter(fio='2')
    serializer_class = PositionSerializer

class PositionArgsApiView(generics.ListCreateAPIView):

    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    http_method_names = ['get',]

#    def list(self, request):
#        queryset = self.get_queryset('1')
#        serializer = PositionSerializer(queryset, many=True)
#        return Response(serializer.data)

    def get_queryset(self, *args):
        query = self.request.GET.get('fio', *args)
        return super().get_queryset().filter(fio_id=query)

# список инцидентов по РГ
class IncidentArgsApiView(generics.ListCreateAPIView):
    queryset = SmAdmin.objects.all()
    serializer_class = IncidentSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('ASSIGNMENT_GROUP', *args)
        return super().get_queryset().filter(ASSIGNMENT_GROUP=query)
#        return super().get_queryset().filter(ASSIGNMENT_GROUP=query).using("test_db")

# список Workflow
@api_view(['GET', 'POST'])
def WorkFlowArgsApiView(request):
    if request.method == 'GET':
        workflow = Workflow.objects.all()
 
        ticket_id = request.query_params.get('ticket_id', None)
        if ticket_id is not None:
            workflow = workflow.filter(ticket_id__icontains=ticket_id)
 
        workflow_serializer = WorkflowSerializer(workflow, many=True)
        return JsonResponse(workflow_serializer.data, safe=False)
        # 'safe=False' for objects serialization
 
    elif request.method == 'POST':
        workflow_data = JSONParser().parse(request)
        workflow_serializer = WorkflowSerializer(data=workflow_data)
        if workflow_serializer.is_valid():
            workflow_serializer.save()
            return JsonResponse(workflow_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(workflow_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''
class WorkFlowArgsApiView(generics.ListCreateAPIView):
    queryset = Workflow.objects.all()
    serializer_class = WorkflowSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('ticket_id', *args)
        return super().get_queryset().filter(ticket_id=query)
'''
'''
class WorkFlowArgsApiView(generics.ListCreateAPIView):
    queryset = Workflow.objects.all()
    serializer_class = WorkflowSerializer
    http_method_names = ['get', 'post', ]

    if http_method_names == 'get': 
        def get_queryset(self, *args):
            query = self.request.GET.get('ticket_id', *args)
            return super().get_queryset().filter(ticket_id=query)

    elif http_method_names == 'post':
        
'''

# API для фильтра по Направлению, Филиалу, Адресу...
# Фильтр по РГ
class DivisionByDirectionArgsApiView(generics.ListCreateAPIView):
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('direction', *args)
        return super().get_queryset().filter(direction=query)

# Фильтр по Филиалу
class DivisionByBranchArgsApiView(generics.ListCreateAPIView):
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('branch', *args)
        return super().get_queryset().filter(branch=query)
        #branch = self.request.branch
        #return Division.objects.filter(branch=branch)

# фильтр по филиалу для списка ЦОДов
class DatacenterSerializerApiView(generics.ListCreateAPIView):
    queryset = Branch.objects.all()
    serializer_class = DatacenterSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('branch', *args)
        return super().get_queryset().filter(branch=query)

# фильтр по Филиалу и ЦОДу для списка РГ
class DivisionBranchDCSerializerApiView(generics.ListCreateAPIView):
    queryset = DivisionBranchDC.objects.all()
    serializer_class = DivisionBranchDCSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('branch_dc', *args)
        return super().get_queryset().filter(branch_dc=query )


