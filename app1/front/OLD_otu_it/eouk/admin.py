from django.contrib import admin

# Register your models here.

from .models import (Division, Branch, SmAdmin, Person, Position, Workflow, NriFmIt, )

@admin.register(Division)
class DivisionAdmin(admin.ModelAdmin):
    list_display = ( 'is_division', 'service_name', 'direction', 'is_owner', 'is_importance', 'branch')

@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
    list_display = ( 'branch', 'region', 'address', 'datacenter' , 'room', 'rack', 'datacenterschemalink', 'roomschemalink' , 
                     'drpschemalink', 'site_id', 'region_id' , 'room_id', 'rack_id',)


@admin.register(SmAdmin)
class SmAdminAdmin(admin.ModelAdmin):
    list_display = ( 'NUMBER_IM', 'BRIEFDESCRIPTION', 'CMP_STATUS', 'ASSIGNMENT_GROUP', 'AFFECTED_ITEM',)

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ( 'first_name', 'second_name', )

@admin.register(Position)
class PositionAdmin(admin.ModelAdmin):
    list_display = ( 'pos_name', 'division_name', 'fio',)

@admin.register(Workflow)
class WorkflowAdmin(admin.ModelAdmin):
    list_display = ( 'work_begin_time', 'comment_state', 'comment_author', 'ticket_id',)


@admin.register(NriFmIt)
class NriFmItAdmin(admin.ModelAdmin):
    list_display = ('CI', 'CI_ID', 'divice_name','logical_status', 'is_division', )
