from rest_framework import serializers
from eouk.models import (Division, SmAdmin, Branch, Person, Position, )

# сериализатор для рабочих групп
class DivisionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Division
        fields = ('id',
                  'is_division',
                  'service_name',
                  'direction',
                  'is_owner',
                  'is_importance',
                  'branch')
        #fields = '__all__'
        #exclude = ['branch']

# сериализатор оп сервису РГ
class DivisionServiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Division
        fields = ('service_name', )

    def get_service_name(self):
        return self.service_name

# сериализатор оп филиалу РГ
class DivisionBranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Division
        fields = ('branch', )

    def get_branch(self):
        return self.branch

# сериализатор по инцидентам
class SmAdminSerializer(serializers.ModelSerializer):

    class Meta:
        model = SmAdmin
        fields = '__all__'

# test
class IncidentSerializer(serializers.ModelSerializer):
    division = serializers.CharField(source='ASSIGNMENT_GROUP')

    class Meta:
        model = SmAdmin
        fields = '__all__'

# Test persons on positions
class PositionSerializer(serializers.ModelSerializer):
    fio_name = serializers.CharField(source='fio.first_name', )

    class Meta:
        model = Position
#        fields = '__all__'
        fields = ('pos_name', 'division_name', 'fio_name', )

