from django.urls import re_path, path
from eouk import views

urlpatterns = [
#    re_path(r'^api/division$', views.division_list),
    path('api/division/', views.DivisionListCreate.as_view() ),
    path('api/smadmin', views.MultiApiView.as_view()),
    path('api/incident', views.IncidentApiView.as_view()),
    path('api/position', views.PositionApiView.as_view()),
    path('api/posargs', views.PositionArgsApiView.as_view()),
    ### кастомные АПИ для получения данных с параметрами
    path('api/incargs', views.IncidentArgsApiView.as_view()),
    path('api/divdir', views.DivisionByDirectionArgsApiView.as_view()),
    path('api/divbr', views.DivisionByBranchArgsApiView.as_view()),
#    url(r'^api/tutorials/(?P<pk>[0-9]+)$', views.tutorial_detail),
#    url(r'^api/tutorials/published$', views.tutorial_list_published)
]
