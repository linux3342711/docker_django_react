from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status

from eouk.models import (Division, SmAdmin, Position, Person, )
from eouk.serializers import (DivisionSerializer, DivisionServiceSerializer, SmAdminSerializer, 
                              IncidentSerializer, PositionSerializer, DivisionBranchSerializer, )
from rest_framework.decorators import api_view
from rest_framework.response import Response

#
'''
@api_view(['GET'])
def division_list(request):
    divisions = Division.objects.filter(published=True)

    if request.method == 'GET':
        division_serializer = DivisionSerializer(divisions, many=True)
        return JsonResponse(division_serializer.data, safe=False)
'''

#
from rest_framework import generics
# весь список РГ
class DivisionListCreate(generics.ListCreateAPIView):
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer
# весь список по Инцидентам
class IncidentApiView(generics.ListCreateAPIView):
    queryset = SmAdmin.objects.all()
    serializer_class = IncidentSerializer

#
from drf_multiple_model.views import ObjectMultipleModelAPIView

class MultiApiView(ObjectMultipleModelAPIView):
    querylist = [
        {'queryset': SmAdmin.objects.filter(ASSIGNMENT_GROUP='ITi-Operating-Infra'), 'serializer_class': SmAdminSerializer},
#        {'queryset': SmAdmin.objects.filter(ASSIGNMENT_GROUP__in=select), 'serializer_class': SmAdminSerializer},
    ]

# API по должности FK к ФИО
class PositionApiView(generics.ListCreateAPIView):
    queryset = Position.objects.filter(fio='2')
    serializer_class = PositionSerializer

class PositionArgsApiView(generics.ListCreateAPIView):

    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    http_method_names = ['get',]

#    def list(self, request):
#        queryset = self.get_queryset('1')
#        serializer = PositionSerializer(queryset, many=True)
#        return Response(serializer.data)

    def get_queryset(self, *args):
        query = self.request.GET.get('fio', *args)
        return super().get_queryset().filter(fio_id=query)

class IncidentArgsApiView(generics.ListCreateAPIView):
    queryset = SmAdmin.objects.all()
    serializer_class = IncidentSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('ASSIGNMENT_GROUP', *args)
        return super().get_queryset().filter(ASSIGNMENT_GROUP=query).using("test_db")

# API для фильтра по Направлению, Филиалу, Адресу...
# Фильтр по РГ
class DivisionByDirectionArgsApiView(generics.ListCreateAPIView):
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('direction', *args)
        return super().get_queryset().filter(direction=query)

# Фильтр по Филиалу
class DivisionByBranchArgsApiView(generics.ListCreateAPIView):
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer
    http_method_names = ['get',]

    def get_queryset(self, *args):
        query = self.request.GET.get('branch', *args)
        return super().get_queryset().filter(branch=query)



