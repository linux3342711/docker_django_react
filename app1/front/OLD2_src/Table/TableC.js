import React from 'react';

export default props => (
    <table className="table table-striped table-responsive-sm table-hover table-condensed table-sm ">
        <thead>
                <tr>
                  <th >
                    NUMBER  </th>
                  <th > 
                    Описание  </th>
                  <th > 
                    CMP_STATUS  </th>
                  <th > 
                    Владелец ИС  </th>
                  <th >
                    Пострадавшая единица  </th>

                </tr>

        </thead>
        <tbody>
            { props.dataC.map(item =>( 
               <tr key={item.NUMBER_IM} >
                    <td>{item.NUMBER_IM}</td>
                    <td>{item.BRIEFDESCRIPTION}</td>
                    <td>{item.CMP_STATUS}</td>
                    <td>{item.ASSIGNMENT_GROUP}</td>
                    <td>{item.AFFECTED_ITEM}</td>
                </tr>
            ))}
        </tbody>
    </table>
)
