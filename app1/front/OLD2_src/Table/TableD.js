import React from 'react';

export default props => (
    <table className="table table-striped table-responsive-sm table-hover table-condensed table-sm ">
        <thead>
                <tr>
                  <th >
                    Время  </th>
                  <th > 
                    Содержание  </th>
                  <th > 
                    Автор  </th>
                  <th > 
                    Инцидент  </th>
                </tr>

        </thead>
        <tbody>
            { props.dataD.map(item =>( 
               <tr key={item.WORK_BEGIN_TIME + item.TICKEDID_ID} >
                    <td>{item.WORK_BEGIN_TIME}</td>
                    <td>{item.COMMENT_STATE}</td>
                    <td>{item.COMMENT_AUTHOR}</td>
                    <td>{item.TICKEDID_ID}</td>
                </tr>
            ))}
        </tbody>
    </table>
)
