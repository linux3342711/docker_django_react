import React from 'react';

export default ({item}) => (
   <div>
    <p>Выбран сервис <b>{item.service_name + ' ' + item.direction}</b></p>
    <p>
    Описание: <br />
    <textarea defaultValue={item.is_division} />
    </p>

    <p>Владелец сервиса: <b>{item.is_owner}</b></p>
    <p>Важность: <b>{item.is_importance}</b></p>
    <p>Филиал: <b>{item.branch}</b></p>
   </div>
)
