from django.urls import path
from . import views

urlpatterns = [
    path('otu/branch/', views.BranchListCreate.as_view() ),
]
