from django.shortcuts import render
from .models import Branch
from .serializer import BranchSerializer
from rest_framework import generics

# Create your views here.

class BranchListCreate(generics.ListCreateAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer

