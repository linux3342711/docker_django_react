from django.apps import AppConfig


class OtuConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'otu'
