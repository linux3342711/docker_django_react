from django.db import models

# Create your models here.

class Branch(models.Model):
    name = models.CharField(db_column='branch_name', default='Any', max_length=64, unique=True, verbose_name='Branch')
    descr = models.CharField(db_column='description', default='Any', max_length=256)

    class Meta:
        db_table = 'branches'
        verbose_name = ('Branch')
        verbose_name_plural = ('Branches')

    def __str__(self):
        return '%s' & self.name


