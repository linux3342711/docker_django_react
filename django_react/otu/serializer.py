from rest_framework import serializers as ser
from .models import Branch

class BranchSerializer(ser.ModelSerializer):
    class Meta:
        model = Branch
        fields = ('id', 'name', 'descr')


